## Основная часть

### DevOps

В репозитории содержится код проекта на Python. Проект — RESTful API сервис. Ваша задача — автоматизировать сборку образа с выполнением python-скрипта:

1. Образ собирается на основе [centos:7](https://hub.docker.com/_/centos?tab=tags&page=1&ordering=last_updated).
2. Python версии не ниже 3.7.
3. Установлены зависимости: `flask` `flask-jsonpify` `flask-restful`.
4. Создана директория `/python_api`.
5. Скрипт из репозитория размещён в /python_api.
6. Точка вызова: запуск скрипта.
7. При комите в любую ветку должен собираться docker image с форматом имени hello:gitlab-$CI_COMMIT_SHORT_SHA . Образ должен быть выложен в Gitlab registry или yandex registry.   

### Product Owner

Вашему проекту нужна бизнесовая доработка: нужно поменять JSON ответа на вызов метода GET `/rest/api/get_info`, необходимо создать Issue в котором указать:

1. Какой метод необходимо исправить.
2. Текст с `{ "message": "Already started" }` на `{ "message": "Running"}`.
3. Issue поставить label: feature.

### Developer

Пришёл новый Issue на доработку, вам нужно:

1. Создать отдельную ветку, связанную с этим Issue.
2. Внести изменения по тексту из задания.
3. Подготовить Merge Request, влить необходимые изменения в `master`, проверить, что сборка прошла успешно.


### Tester

Разработчики выполнили новый Issue, необходимо проверить валидность изменений:

1. Поднять докер-контейнер с образом `python-api:latest` и проверить возврат метода на корректность.
2. Закрыть Issue с комментарием об успешности прохождения, указав желаемый результат и фактически достигнутый.

## Итог

В качестве ответа пришлите подробные скриншоты по каждому пункту задания:

- файл gitlab-ci.yml: [gitlab-ci](https://gitlab.com/mamaiifortesting/netology-cicd/-/blob/main/.gitlab-ci.yml?ref_type=heads)
 
 ```
 stages:
  - build
  - deploy
image: docker:20.10.5
services:
  - docker:20.10.5-dind
build-job:       # This job runs in the build stage, which runs first.
  stage: build
  script:
    - docker build -t hello:gitlab-$CI_COMMIT_SHORT_SHA .
  except:
    - main
deploy-job:      # This job runs in the deploy stage.
  stage: deploy  # It only runs when *both* jobs in the test stage complete successfully.
  script:
    - docker build -t $CI_REGISTRY/mamaiifortesting/netology-cicd/python-api:latest .
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker push $CI_REGISTRY/mamaiifortesting/netology-cicd/python-api:latest
  only:
    - main
```
- Dockerfile: [Dockerfile](https://gitlab.com/mamaiifortesting/netology-cicd/-/blob/main/Dockerfile?ref_type=heads)

```
FROM centos:7

RUN yum install python3 python3-pip -y
COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt
COPY python-api.py python-api.py
CMD ["python3", "python-api.py"]
```

- лог успешного выполнения пайплайна: [МР с успешными пайпами на билд и деплой](https://gitlab.com/mamaiifortesting/netology-cicd/-/merge_requests/2)
- решённый Issue: [Issue](https://gitlab.com/mamaiifortesting/netology-cicd/-/issues/1)